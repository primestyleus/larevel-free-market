<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\LikeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ログイン, ユーザー登録
Auth::routes();

// トップページ
Route::get('/', [ItemController::class, 'index'])->name('top');

 // プロフィール詳細
Route::get('users/{user}', [UserController::class, 'show'])->name('users.show');

// プロフィール編集
Route::get('profile/edit', [UserController::class, 'edit'])->name('profile.edit');
Route::patch('profile/edit', [UserController::class, 'update'])->name('profile.update');

// プロフィール画像編集
Route::get('profile/edit_image', [UserController::class, 'editImage'])->name('profile.edit_image');
Route::patch('profile/edit_image', [UserController::class, 'updateImage'])->name('profile.update_image');

// 出品商品一覧
Route::get('users/{user}/exhibitions', [UserController::class, 'itemList'])->name('users.posts');

// 新規出品
// 商品情報編集
// 商品詳細
// 商品削除
Route::resource('items', ItemController::class,  ['except' => ['index']]);

// 商品画像編集
Route::get('items/{item}/edit_image', [ItemController::class, 'editImage'])->name('items.edit_image');
Route::patch('items/{item}/edit_image', [ItemController::class, 'updateImage'])->name('items.update_image');

// 購入確認
Route::post('items/{item}/confirm', [ItemController::class, 'confirm'])->name('items.confirm');
Route::post('items/{item}/finish', [ItemController::class, 'finish'])->name('items.finish');


// 購入確認
Route::post('items/{item}/confirm', [ItemController::class, 'confirm'])->name('items.confirm');
Route::post('items/{item}/finish', [ItemController::class, 'finish'])->name('items.finish');

// お気に入り一覧
// お気に入り登録
// お気に入り削除
Route::resource('likes', LikeController::class,  ['only' => ['index', 'store']]);
Route::delete('likes', [LikeController::class, 'destroy']);
