<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Item Name') }}</label>
    <div class="col-md-6">
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $item->name) }}" required autocomplete="name" autofocus>
        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Item Description') }}</label>

    <div class="col-md-6">
        <textarea rows="5" id="description" class="form-control @error('description') is-invalid @enderror" name="description" required>{{ old('description', $item->description) }}</textarea>

        @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>
    <div class="col-md-6">
        <input id="price" type="number" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price', $item->price) }}" required>
        @error('price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="category_id" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>
    <div class="col-md-6">
        <select id="category_id" class="form-control @error('category_id') is-invalid @enderror" name="category_id" required>
            <option value="">選択してください</option>
            @foreach($categories as $category)
                <option value="{{ $category->id }}"
                    @if($category->id === (int) old('category_id', $item->category_id))
                        selected
                    @endif
                >{{ $category->name }}</option>
            @endforeach
        </select>
        @error('category_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

@if (!(isset($item) && $item->id))
<div class="form-group row">
    <label for="profile" class="col-md-4 col-form-label text-md-right">{{ __('Choose Image') }}</label>

    <div class="col-md-6">
        <input type="file" name="image" class="form-control-file @error('image') is-invalid @enderror">
        @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
@endif

<div class="form-group row mb-0">
    <div class="col-md-8 offset-md-4">
        <button type="submit" class="btn btn-primary">
            {{ __('Post') }}
        </button>
    </div>
</div>
