<?php
use App\Models\Item;
use Illuminate\Support\Facades\Auth;
?>
@extends('layouts.app')

@section('title', __('Item Detail'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <h1>@yield('title')</h1>
            @include('shared.item_detail')
            @if ($item->user_id === Auth::id())
                <a class="btn btn-block btn-outline-primary" href="{{ route('items.edit', ['item' => $item]) }}">
                    <i aria-hidden="true" class="fas fa-edit"></i>
                    {{ __('Edit') }}
                </a>
            @elseif($item->status === Item::STATUS_OPEN)
            <form method="POST" action="{{ route('items.confirm', ['item' => $item]) }}">
                @csrf
                <button class="btn btn-block btn-primary">{{ __('Check Out') }}</button>
            </form>
            @else
                <button class="btn btn-secondary btn-block" disabled>{{ __('Sold Out') }}</button>
            @endif
        </div>
    </div>
</div>
@endsection
