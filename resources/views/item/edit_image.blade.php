@extends('layouts.app')

@section('title', __('Edit Item Image'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <h1 class="card-header">@yield('title')</h1>

                <div class="card-body">
                    <form method="POST" action="{{ route('items.update_image', ['item' => $item->id]) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        @include('shared.edit_image', ['image' => $item->image])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
