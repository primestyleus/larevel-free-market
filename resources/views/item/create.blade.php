@extends('layouts.app')

@section('title', __('Post Item'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <h1 class="card-header">@yield('title')</h1>

                <div class="card-body">
                    <form method="POST" action="{{ route('items.store') }}" enctype="multipart/form-data">
                        @csrf
                        @include('item._form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
