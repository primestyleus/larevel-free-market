@extends('layouts.app')

@section('title', __('Thank you for your purchase'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <h1>@yield('title')</h1>
            @include('shared.item_detail')

            <a href="{{ route('top') }}">{{ __('Back to Top') }}</a>
        </div>
    </div>
</div>
@endsection
