@extends('layouts.app')

@section('title', __('Confirm Check Out'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <h1>@yield('title')</h1>
            @include('shared.item_detail')
            <form method="POST" action="{{ route('items.finish', ['item' => $item->id]) }}">
                @csrf
                <button class="btn btn-block btn-primary">{{ __('I Confirmed, Proceed to Check Out') }}</button>
            </form>
        </div>
    </div>
</div>
@endsection
