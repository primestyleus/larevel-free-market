@extends('layouts.app')

@section('title', __('Profile'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8 mt-3">
            <div class="card">
                <h1 class="card-header">@yield('title')</h1>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            @if ($user->image)
                                <img src="{{ asset('storage/' . $user->image) }}" class="img-fluid rounded">
                            @else
                                <img src="{{ asset('img/noimage.png') }}" class="img-fluid rounded">
                            @endif
                            @if ($user->id === Auth::id())
                                <a href="{{ route('profile.edit_image') }}">
                                    <i class="fas fa-image"></i>
                                    {{ __('Change this image.') }}
                                </a>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <p class="lead">{{ $user->name }}</p>
                            <p>{{ $user->profile }}</p>
                            <p>{{ __('Posted Items') }}: {!! number_format($user->items->count()) !!}</p>
                            @if ($user->id === Auth::id())
                                <a class="" href="{{ route('profile.edit') }}">
                                    <i class="fas fa-user-edit"></i>
                                    {{ __('Edit Profile') }}
                                </a>
                            @endif
                        </div>
                    </div>
                    @if ($user->id === Auth::id())
                    <h2 class="mt-5">{{ __('Orders') }}</h2>
                    <ul class="list-group">
                        @forelse ($user->orders as $order)
                            <li class="list-group-item">
                                <small class="pr-2">{{ $order->item->created_at->format('Y.m.d H:i') }}</small>
                                <a class="font-weight-bold" href="{{ route('items.show', ['item' => $order->item]) }}">
                                    {{ $order->item->name }}
                                </a>
                                :
                                {{ number_format($order->item->price) }}円
                                <small class="pl-2">
                                    出品者
                                    <a href="{{ route('users.show', ['user' => $order->item->user_id]) }}">
                                    {{ $order->item->user->name }}さん
                                    </a>
                                </small>
                            </li>
                        @empty
                            <li class="list-group-item">履歴はありません。</li>
                        @endforelse
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
