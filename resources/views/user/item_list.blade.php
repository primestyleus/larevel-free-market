@extends('layouts.app')

@section('title', __(':name\'s Item List', ['name' => $user->name]))

@section('content')
<div class="container">
        <h1>@yield('title')</h1>
        <div class="text-right mb-3">
            <a class="btn btn-success" href="{{ route('items.create') }}">{{ __('New Item') }}</a>
        </div>
        @include('shared.item_list', ['items' => $user->items()->get(), 'control' => true])
    </div>
</div>
@endsection
