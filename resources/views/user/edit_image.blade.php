@extends('layouts.app')

@section('title', __('Edit Profile Image'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <h1 class="card-header">@yield('title')</h1>

                <div class="card-body">
                    <form method="POST" action="{{ route('profile.update_image') }}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        @include('shared.edit_image', ['image' => $user->image])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
