@extends('layouts.app')

@section('title', __('Favorite Items'))

@section('content')
<div class="container">
    <h1>@yield('title')</h1>
    @if (count($items) === 0)
    <p class="alert alert-warning px-3" role="alert">{{ __('There are no items.') }}</p>
    @else
    @include('shared.item_list', ['control' => false])
    @endif
</div>
@endsection
