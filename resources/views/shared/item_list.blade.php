<?php
use App\Models\Item;
?>
@if (count($items) === 0)
<p class="alert alert-warning px-3" role="alert">{{ __('There are no items.') }}</p>
@else
<ul class="row list-unstyled">
    @foreach ($items as $key => $item)
    <li class="col-md-6 col-lg-4 py-3">
        <div class="card">
            <div class="card-header">
                @if($item->status === Item::STATUS_OPEN)
                    <span class="badge badge-success float-right mt-1">出品中</span>
                @else
                    <span class="badge badge-warning float-right mt-1">売り切れ</span>
                @endif
                {{ $item->name }}
            </div>
            <div class="card-body">
                <a href="{{ route('items.show', ['item' => $item->id]) }}">
                    <img src="{{ asset('storage/' . $item->image) }}" class="img-fluid rounded">
                </a>
                <div class="row mt-2">
                    <div class="col">
                        @if (!$control)
                        <form class="d-inline-block" method="POST" action="{{ route('likes.store') }}">
                            @csrf
                            <input type="hidden" name="item_id" value="{{ $item->id }}">
                            <a href="javascript:void(0)" onClick="this.parentElement.submit();">
                            @if(Auth::user()->isItemLiked($item->id))
                                @method('delete')
                                <i class="fas fa-lg fa-star text-danger"></i>
                            @else
                                <i class="far fa-lg fa-star text-danger"></i>
                            @endif
                            </a>
                        </form>
                        @endif
                        <span class="font-weight-bold">{{ number_format($item->price) }}円</span>
                    </div>
                    <div class="col-auto text-right">
                        <small>
                            @if($control)
                                {{ $item->updated_at }}
                            @else
                                {{ $item->created_at->format('Y.m.d') }}
                            @endif
                        </small>
                    </div>
                </div>
                <p class="my-1 small">カテゴリ: {{ $item->category->name }}</p>
                @if (!$control)
                <p class="my-1 small">
                    出品者:
                    <a href="{{ route('users.show', ['user' => $item->user_id]) }}">
                        {{ $item->user->name }}さん
                    </a>
                </p>
                @endif
                <p class="mb-0">{{ $item->description }}</p>
                @if ($control)
                <ul class="row list-unstyled mt-1">
                    <li class="col-auto pt-1">
                        <a href="{{ route('items.edit', ['item' => $item->id]) }}" class=""><i class="fas fa-edit"></i>
                            {{ __('Edit') }}</a>
                    </li>
                    <li class="col-auto pt-1">
                        <a href="{{ route('items.edit_image', ['item' => $item->id]) }}" class=""><i class="fas fa-image"></i>
                            {{ __('Edit Image') }}</a>
                    </li>
                    <li class="col text-right">
                        <form method="POST" action="{{ route('items.destroy', ['item' => $item->id]) }}">
                            @csrf
                            @method('delete')
                            <button class="btn btn-sm btn-danger">
                                <i class="fas fa-trash-alt"></i>
                                {{ __('Delete') }}
                            </button>
                        </form>
                    </li>
                </ul>
                @endif
            </div>
        </div>
    </li>
    @endforeach
</ul>
@endif
