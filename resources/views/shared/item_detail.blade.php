<dl class="row my-4">
    <dt class="col-md-4 text-md-right">{{ __('Item Name') }}</dt>
    <dd class="col-md-8">{{ $item->name }}</dd>
    <dt class="col-md-4 text-md-right">{{ __('Image') }}</dt>
    <dd class="col-md-8"><img src="{{ asset('storage/' . $item->image) }}" class="img-fluid rounded"></dd>
    <dt class="col-md-4 text-md-right">{{ __('Category') }}</dt>
    <dd class="col-md-8">{{ $item->category->name }}</dd>
    <dt class="col-md-4 text-md-right">{{ __('Price') }}</dt>
    <dd class="col-md-8">{{ number_format($item->price) }}円</dd>
    <dt class="col-md-4 text-md-right">{{ __('Item Description') }}</dt>
    <dd class="col-md-8">{{ $item->description }}</dd>
</dl>
