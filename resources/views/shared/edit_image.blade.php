<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">{{ __('Current Image') }}</label>
    <div class="col-md-6">
        @if ($image)
            <img src="{{ asset('storage/' . $image) }}" class="img-fluid rounded">
        @else
            <img src="{{ asset('img/noimage.png') }}" class="img-fluid rounded">
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="profile" class="col-md-4 col-form-label text-md-right">{{ __('Choose Image') }}</label>

    <div class="col-md-6">
        <input type="file" name="image" class="form-control-file @error('image') is-invalid @enderror">
        @error ('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-8 offset-md-4">
        <button type="submit" class="btn btn-primary">
            {{ __('Update') }}
        </button>
    </div>
</div>
