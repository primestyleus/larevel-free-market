<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:1000'],
            'category_id' => ['required', 'exists:categories,id'],
            'price' => ['required', 'digits_between:0,6'],
        ];

        // 新規出品時のみ画像もチェック
        if (empty($this->item->id)) {
            $rules['image'] = [
                'image',
                'mimes:jpeg,png',
                'dimensions:min_width=50,min_height=50,max_width=1000,max_height=1000'
            ];
        }
        return $rules;
    }
}
