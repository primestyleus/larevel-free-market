<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * お気に入り一覧を表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('like.index', [
            'items' => Auth::user()->likedItems,
        ]);
    }

    /**
     * お気に入りへの追加処理を実行
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item_id = $request->input('item_id', 0);

        // 商品が存在しない場合は404
        if (Item::where('id', $item_id)->count() === 0) {
            abort(404);
        }

        // レコードが存在しない場合のみ追加
        Like::firstOrCreate([
            'user_id' => Auth::id(),
            'item_id' => $item_id
        ]);
        return back();
    }

    /**
     * お気に入りの削除処理を実行
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $item_id = $request->input('item_id', 0);

        // 対象のレコードが存在する場合のみ削除
        $like = Like::firstWhere([
            'user_id' => Auth::id(),
            'item_id' => $item_id
        ]);
        if (!empty($like)) {
            $like->delete();
        }
        return back();
    }
}
