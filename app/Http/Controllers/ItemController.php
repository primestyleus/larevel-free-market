<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageChangeRequest;
use App\Http\Requests\ItemRequest;
use App\Models\Category;
use App\Models\Item;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 商品一覧を表示
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('item.index', [
            'items' => Item::with(['user', 'category'])->get(),
        ]);
    }

    /**
     * 出品画面を表示
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item.create', [
            'item' => new Item,
            'categories' => Category::all()
        ]);
    }

    /**
     * 出品処理を実行
     *
     * @param  ItemRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        $path = null;
        $image = $request->file('image', false);
        if ($image !== false) {
            $path = $image->store('items/' . Auth::id(), ['disk' => 'public']);
        }
        $data = $request->only(['name', 'description', 'price', 'category_id']) + [
            'image' => $path,
            'user_id' => Auth::id(),
            'status' => Item::STATUS_OPEN
        ];
        $result = Item::create($data);
        if ($result) {
            return redirect()
                    ->route('users.posts', ['user' => Auth::id()])
                    ->with('success', __('Saved successfully.'));
        } else {
            return back()
                    ->with('error', __('Something went wrong, please try again.'));
        }
    }

    /**
     * 商品詳細を表示
     *
     * @param  Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return view('item.show', [
            'item' => $item
        ]);
    }

    /**
     * 商品の編集画面を表示
     *
     * @param  Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        if ($item->user_id !== Auth::id()) {
            abort(403);
        }
        return view('item.edit', [
            'item' => $item,
            'categories' => Category::all()
        ]);
    }

    /**
     * 商品の編集処理を実行
     *
     * @param  ItemRequest  $request
     * @param  Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request, Item $item)
    {
        if ($item->user_id !== Auth::id()) {
            abort(403);
        }
        $result = $item->update(
            $request->only(['name', 'description', 'price', 'category_id'])
        );
        if ($result) {
            return redirect()
                    ->route('users.posts', ['user' => Auth::id()])
                    ->with('success', __('Saved successfully.'));
        } else {
            return back()
                    ->with('error', __('Something went wrong, please try again.'));
        }
    }

    /**
     * 商品画像の編集画面を表示
     *
     * @param  Item  $item
     * @return \Illuminate\Http\Response
     */
    public function editImage(Item $item)
    {
        if ($item->user_id !== Auth::id()) {
            abort(403);
        }
        return view('item.edit_image', [
            'item' => $item
        ]);
    }

    /**
     * 商品画像の編集処理
     *
     * @param  ImageChangeRequest  $request
     * @param  Item  $item
     * @return \Illuminate\Http\Response
     */
    public function updateImage(ImageChangeRequest $request, Item $item)
    {
        if ($item->user_id !== Auth::id()) {
            abort(403);
        }

        $result = true;
        $message = '';

        $image = $request->file('image', false);
        if ($image !== false && $path = $image->store('items/' . Auth::id(), ['disk' => 'public'])) {
            $oldImage = $item->image;
            $result = $item->update(['image' => $path]);
            if ($result) {
                $message = __('Saved successfully.');
                if (!empty($oldImage)) {
                    Storage::disk('public')->delete($oldImage);
                }
            }
        }
        if ($result) {
            return redirect()
                    ->route('users.posts', ['user' => Auth::id()])
                    ->with('success', $message);
        } else {
            return back()
                    ->with('error', __('Something went wrong, please try again.'));
        }
    }

    /**
     * 商品を削除
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        if ($item->user_id !== Auth::id()) {
            abort(403);
        }
        if($item->delete()) {
            return back()->with('success', __('Deleted successfully.'));
        } else {
            return back()->with('error', __('Something went wrong, please try again.'));
        }

    }

    /**
     * 購入確認画面を表示
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Item  $item
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request, Item $item)
    {
        if ($item->status !== Item::STATUS_OPEN) {
            abort(404);
        }
        return view('item.confirm', [
            'item' => $item,
        ]);
    }

    /**
     * 購入処理を行い、完了画面を表示
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Item  $item
     * @return \Illuminate\Http\Response
     */
    public function finish(Request $request, Item $item)
    {
        if ($item->status !== Item::STATUS_OPEN) {
            abort(404);
        }

        // トランザクション
        try {
            DB::transaction(function () use ($item) {

                // 売り切れに変更
                $item->update(['status' => Item::STATUS_SOLD]);

                // 履歴を保存
                $order = Order::create([
                    'user_id' => Auth::id(),
                    'item_id' => $item->id
                ]);
            });

            // 正常時は完了画面へ
            return view('item.finish', [
                'item' => $item,
            ]);

        } catch (\Exception $e) {

            // エラーのため商品詳細へもどる
            return redirect()
                    ->route('items.show', ['item' => $item])
                    ->with('error', __('Ckeck out failed.'));
        }
    }
}
