<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ImageChangeRequest;
use App\Http\Requests\ProfileRequest;
use App\Models\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * プロフィールを表示
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('user.show', [
            'user' => $user,
        ]);
    }

    /**
     * 出品中商品の一覧を表示
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function itemList(User $user)
    {
        return view('user.item_list', [
            'user' => $user
        ]);
    }

    /**
     * プロフィール編集画面を表示
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('user.edit', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * プロフィール編集処理を実行
     *
     * @param  ProfileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request)
    {
        $user = Auth::user();
        if ($user->update($request->all())) {
            return redirect()
                    ->route('users.show', ['user' => $user])
                    ->with('success', __('Saved successfully.'));
        } else {
            return back()
                    ->with('error', __('Something went wrong, please try again.'));
        }
    }

    /**
     * 画像編集フォームを表示
     *
     * @return void
     */
    public function editImage()
    {
        return view('user.edit_image', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * プロフィール画像編集処理を実行
     *
     * @param  ImageChangeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function updateImage(ImageChangeRequest $request)
    {
        $result = true;
        $message = '';

        $user = Auth::user();
        $image = $request->file('image', false);
        if ($image !== false && $path = $image->store('users/' . $user->id, ['disk' => 'public'])) {
            $oldImage = $user->image;
            $result = $user->update(['image' => $path]);
            if ($result) {
                $message = __('Saved successfully.');
                if (!empty($oldImage)) {
                    Storage::disk('public')->delete($oldImage);
                }
            }
        }
        if ($result) {
            return redirect()
                    ->route('users.show', ['user' => $user])
                    ->with('success', $message);
        } else {
            return back()
                    ->with('error', __('Something went wrong, please try again.'));
        }
    }
}
